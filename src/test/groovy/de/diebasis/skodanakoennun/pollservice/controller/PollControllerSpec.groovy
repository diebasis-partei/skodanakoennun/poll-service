package de.diebasis.skodanakoennun.pollservice.controller

import de.diebasis.skodanakoennun.models.Poll
import de.diebasis.skodanakoennun.models.PollStatus
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import spock.lang.Specification

class PollControllerSpec extends Specification {

    def pollController = new PollController()

    def "Create a poll successfully"() {
        given:
        def poll = new Poll()

        when:
        ResponseEntity<Void> response = pollController.addPoll(poll)

        then:
        assert response.getStatusCode() == HttpStatus.OK
    }

    def "Creation of poll failed due to wrong input"() {
        given:
        def poll = new Poll(id: 4711)

        when:
        ResponseEntity<Void> response = pollController.addPoll(poll)

        then:
        assert response.getStatusCode() == HttpStatus.BAD_REQUEST
    }

    def "Finds a poll by status successfully"() {
        when:
        List<Poll> activePolls = pollController.findPollsByStatus(PollStatus.ACTIVE)

        then:
        assert !activePolls.isEmpty()
    }

    def "Find a poll by id successfully"() {
        when:
        Poll poll  = pollController.getPollById(47)

        then:
        assert poll != null
    }

    def "Updates a poll by id and payload successfully"() {
        when:
        Poll poll  = pollController.updatePollById(47, new Poll())

        then:
        assert poll != null
    }

    def "Archive a poll by id"() {
        when:
        def response = pollController.archivePoll(47)

        then:
        assert response.getStatusCode() == HttpStatus.NO_CONTENT
    }

}
