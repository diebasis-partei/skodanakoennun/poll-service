package de.diebasis.skodanakoennun.pollservice.configuration


import org.springframework.security.oauth2.jwt.Jwt
import spock.lang.Specification

class KeycloakRealmRoleConverterSpec extends Specification {

    def converter = new KeycloakRealmRoleConverter()

    def "Should convert a jwt by keycloak properly"() {
        given:
        def roles = [roles: ['poll:access', 'poll:write']]
        def jwt = Mock(Jwt.class)
        jwt.getClaims() >> [realm_access: roles]
        jwt.getClaimAsMap("realm_access") >> roles

        when:
        def result = converter.convert(jwt)

        then:
        assert result.size() > 0
        assert result.get(0).getProperties().get("authority") == "ROLE_poll:access"
    }

    def "Should not contain any claims"() {
        given:
        def jwt = Mock(Jwt.class)
        jwt.getClaims() >> null

        when:
        def result = converter.convert(jwt)

        then:
        assert result.size() == 0
    }

    def "Should not contain realm access claim"() {
        given:
        def jwt = Mock(Jwt.class)
        jwt.getClaims() >> [client_access: []]

        when:
        def result = converter.convert(jwt)

        then:
        assert result.size() == 0
    }

    def "Should contain realm access claim with empty map"() {
        given:
        def jwt = Mock(Jwt.class)
        jwt.getClaims() >> [realm_access: Collections.emptyMap()]
        jwt.getClaimAsMap('realm_access') >> [realm_access: Collections.emptyMap()]

        when:
        def result = converter.convert(jwt)

        then:
        assert result.size() == 0
    }
}
