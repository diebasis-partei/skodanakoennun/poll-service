package de.diebasis.skodanakoennun.pollservice.configuration;

import org.springframework.core.convert.converter.Converter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.jwt.Jwt;

import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class KeycloakRealmRoleConverter implements Converter<Jwt, Collection<GrantedAuthority>> {

    private static final String REALM_ACCESS = "realm_access";

    @Override
    public Collection<GrantedAuthority> convert(Jwt jwt) {
        final Map<String, Object> realmAccess = new HashMap<>();
        if (jwt.getClaims() != null && jwt.getClaims().containsKey(REALM_ACCESS)) {
            realmAccess.putAll(jwt.getClaimAsMap(REALM_ACCESS));
        }

        final List<GrantedAuthority> roles = ((List<String>) realmAccess.getOrDefault("roles", Collections.emptyList()))
            .stream()
            .map(role -> "ROLE_" + role)
            .map(SimpleGrantedAuthority::new)
            .collect(Collectors.toList());
        return roles;
    }

}
