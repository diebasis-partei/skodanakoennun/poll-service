package de.diebasis.skodanakoennun.pollservice.configuration;

import io.swagger.models.Scheme;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;

import java.util.Collections;

@Configuration
public class OpenApiConfig {

    private static final String LONG_DESCRIPTION_MD = "";

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.OAS_30)
            .protocols(Collections.singleton(Scheme.HTTPS.toValue()))
            .useDefaultResponseMessages(false)
            .apiInfo(new ApiInfo("Poll API documentation for Skodanakoennun",
                LONG_DESCRIPTION_MD,
                "v1",
                null,
                new Contact(
                    "dieBasis - Partei",
                    "https://diebasis-partei.de",
                    "it@diebasis-partei.de"
                ),
                "MIT",
                "https://gitlab.com/diebasis-partei/skodanakoennun/poll-service/-/blob/master/LICENSE",
                Collections.emptyList()))
            .select()
            .apis(RequestHandlerSelectors.any())
            .paths(PathSelectors.regex("\\/error|\\/internal.*|\\//actuator.*").negate())
            .build();
    }

}
