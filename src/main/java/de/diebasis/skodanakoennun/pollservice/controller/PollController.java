package de.diebasis.skodanakoennun.pollservice.controller;

import de.diebasis.skodanakoennun.models.Poll;
import de.diebasis.skodanakoennun.models.PollStatus;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/poll")
@Slf4j
@Api(tags = "Poll")
public class PollController {

    /**
     * Creates a new poll using the given body.
     *
     * @param poll the poll object to create
     * @return ResponseEntity
     */
    @ApiOperation("Receives a post request with a poll as body element to create it.")
    @ApiResponses(value = {
        @ApiResponse(responseCode = "201", description = "Poll successfully created"),
        @ApiResponse(responseCode = "400", description = "Invalid request, maybe not all required poll parameter given")
    })
    @PostMapping
    @PreAuthorize("hasRole('poll:write')")
    public ResponseEntity<Void> addPoll(@Valid @RequestBody Poll poll) {
        if (poll.getId() > 0) {
            return ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok().build();
    }

    /**
     * Finds {@link Poll} by {@link PollStatus} and returns a {@link List}.
     *
     * @param pollStatus the status to search for
     * @return List of Polls
     */
    @GetMapping("/findByStatus")
    @PreAuthorize("hasRole('poll:read')")
    public List<Poll> findPollsByStatus(@RequestParam("status") PollStatus pollStatus) {
        return Collections.singletonList(new Poll());
    }

    /**
     * Searches for a poll using it's Id.
     *
     * @param pollId the poll id
     * @return Poll the poll
     */
    @GetMapping("/{pollId}")
    @PreAuthorize("hasRole('poll:read')")
    public Poll getPollById(@PathVariable int pollId, Principal principal) {
        final var poll = new Poll();
        poll.setId(pollId);
        return poll;
    }

    /**
     * Updates for a poll using it's Id and the given payload.
     *
     * @param pollId the poll id
     * @param poll the payload to update
     * @return Poll the poll
     */
    @PutMapping("/{pollId}")
    @PreAuthorize("hasRole('poll:write')")
    public Poll updatePollById(@PathVariable int pollId, @RequestBody Poll poll, Principal principal) {
        log.info("principal {}", principal);
        return new Poll();
    }

    /**
     * Archives an poll using it's id.
     *
     * @param pollId the poll id
     * @return Poll the poll
     */
    @DeleteMapping("/{pollId}")
    @PreAuthorize("hasRole('poll:read')")
    public ResponseEntity<Void> archivePoll(@PathVariable int pollId) {

        return ResponseEntity.noContent().build();
    }

}
